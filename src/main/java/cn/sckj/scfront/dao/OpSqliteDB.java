package cn.sckj.scfront.dao;


import cn.sckj.scfront.util.ConfigProperties;

import java.sql.*;
import java.text.SimpleDateFormat;

public class OpSqliteDB {

    private static final String Class_Name = "org.sqlite.JDBC";

    // 创建Sqlite数据库连接
    public static Connection createConnection() throws SQLException, ClassNotFoundException {
        Class.forName(Class_Name);
        ConfigProperties configProperties = new ConfigProperties();
        return DriverManager.getConnection(configProperties.getDatabase_url());
    }


    public static boolean saveInfo(String message){
        boolean result = false;
        Connection connection = null;
        Statement statement = null;
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            connection = createConnection();
            String recdate = sdf.format(new java.util.Date());
            String sql = "insert into ComputerData(computerinfo,recdate,recdate_c) values('"+message+"','"+recdate+"','"+recdate.substring(0,9)+")";
            statement = connection.createStatement();
            statement.executeUpdate(sql);
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            try {
                statement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
