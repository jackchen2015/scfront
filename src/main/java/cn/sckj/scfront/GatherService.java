package cn.sckj.scfront;

import cn.sckj.scfront.entity.ComputerInfo;
import cn.sckj.scfront.entity.DiskInfo;
import cn.sckj.scfront.entity.GatherInfo;
import cn.sckj.scfront.entity.MemoryInfo;
import org.hyperic.sigar.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.*;

public class GatherService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GatherService.class);

    public GatherInfo gather(){
        GatherInfo gatherInfo = new GatherInfo();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        gatherInfo.setGaterDate(sdf.format(new Date()));
        gatherInfo.setComputerInfo(gatherCumputer());
        gatherInfo.setCpuInfo(gatherCpu());
        gatherInfo.setMemoryInfo(gatherMemory());
        gatherInfo.setDiskInfoList(gatherDisk());
        return gatherInfo;
    }

    /**
     * 收集系统信息
     * @return
     */
    private ComputerInfo gatherCumputer() {
        ComputerInfo computerInfo = new ComputerInfo();
        try {
            Properties props = System.getProperties();
            InetAddress addr;
            addr = InetAddress.getLocalHost();
            String ip = addr.getHostAddress();
            Map<String, String> map = System.getenv();
            computerInfo.setComputername(map.get("COMPUTERNAME"));
            computerInfo.setUsername(map.get("USERNAME"));
            computerInfo.setHostname(addr.getHostName());
            computerInfo.setIp(ip);
            computerInfo.setOsname(props.getProperty("os.name"));
            computerInfo.setOsversion(props.getProperty("os.version"));
            OperatingSystem OS = OperatingSystem.getInstance();
            computerInfo.setOsvendername(OS.getVendorName());
            computerInfo.setOsdesc(OS.getDescription());
            computerInfo.setOsarch(OS.getArch());
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return computerInfo;
    }

    /**
     * 收集CPU信息
     * @return
     */
    private cn.sckj.scfront.entity.CpuInfo gatherCpu(){
        cn.sckj.scfront.entity.CpuInfo cpuInfo = new cn.sckj.scfront.entity.CpuInfo();
        try {
            Sigar sigar = new Sigar();
            CpuInfo infos[] = sigar.getCpuInfoList();
            CpuPerc perc = sigar.getCpuPerc();
            cpuInfo.setIdle(String.valueOf(perc.getIdle()));
            cpuInfo.setUseruse(String.valueOf(perc.getUser()));
            cpuInfo.setWait(String.valueOf(perc.getWait()));
            cpuInfo.setCombined(String.valueOf(perc.getCombined()));
            cpuInfo.setNice(String.valueOf(perc.getNice()));
            cpuInfo.setSysuse(String.valueOf(perc.getSys()));
            cpuInfo.setMhz(infos[0].getMhz()+"");
            cpuInfo.setVendor(infos[0].getVendor());
            cpuInfo.setModel(infos[0].getModel());
            cpuInfo.setCachesize(infos[0].getCacheSize()+"");
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return cpuInfo;
    }

    /**
     * 收集内存信息
     * @return
     */
    private MemoryInfo gatherMemory(){
        MemoryInfo memoryInfo = new MemoryInfo();
        try{
            Sigar sigar = new Sigar();
            Mem mem = sigar.getMem();
            memoryInfo.setTotal(String.valueOf(mem.getTotal() / (1024L*1024L)));//"M"
            memoryInfo.setUsed(String.valueOf(mem.getUsed() / (1024L*1024L)));
            memoryInfo.setFree(String.valueOf(mem.getFree() / (1024L*1024L)));
            Swap swap = sigar.getSwap();
            memoryInfo.setSwaptotal(String.valueOf(swap.getTotal() / (1024L*1024L)));
            memoryInfo.setUsed(String.valueOf(swap.getUsed()/ (1024L*1024L)));
            memoryInfo.setFree(String.valueOf(swap.getFree()/ (1024L*1024L)));
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return memoryInfo;
    }

    private List<DiskInfo> gatherDisk(){
        List<DiskInfo> diskInfos = new ArrayList<DiskInfo>();
        try{
            Sigar sigar = new Sigar();
            FileSystem fslist[] = sigar.getFileSystemList();
            for (int i = 0; i < fslist.length; i++) {
                FileSystem fs = fslist[i];
                FileSystemUsage usage = null;
                usage = sigar.getFileSystemUsage(fs.getDirName());
                switch (fs.getType()) {
                    case 0: // TYPE_UNKNOWN ：未知
                        break;
                    case 1: // TYPE_NONE
                        break;
                    case 2: // TYPE_LOCAL_DISK : 本地硬盘
                        DiskInfo diskInfo = new DiskInfo();
                        diskInfo.setDevname(fs.getDevName());
                        diskInfo.setDevtype(fs.getSysTypeName());
                        diskInfo.setTotal(String.valueOf(usage.getTotal()/(1024*1024)));//"GB"
                        diskInfo.setAvail(String.valueOf(usage.getAvail()/(1024*1024)));//"GB"
                        diskInfo.setFree(String.valueOf(usage.getFree()/(1024*1024)));//"GB"
                        diskInfos.add(diskInfo);
                        break;
                    case 3:// TYPE_NETWORK ：网络
                        break;
                    case 4:// TYPE_RAM_DISK ：闪存
                        break;
                    case 5:// TYPE_CDROM ：光驱
                        break;
                    case 6:// TYPE_SWAP ：页面交换
                        break;
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return diskInfos;
    }
}
