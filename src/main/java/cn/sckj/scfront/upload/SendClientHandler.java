package cn.sckj.scfront.upload;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class SendClientHandler extends SimpleChannelInboundHandler<String> {

    protected void channelRead0(ChannelHandlerContext channelHandlerContext, String message) throws Exception {
        System.out.println("Server say : " + message);
    }

    @Override
     public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("Client active ");
         super.channelActive(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("Client close ");
         super.channelInactive(ctx);
    }
}
