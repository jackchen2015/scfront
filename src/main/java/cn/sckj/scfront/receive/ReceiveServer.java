package cn.sckj.scfront.receive;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * 功能简介: 接收局域网内其他客户端收集的信息
 * 功能说明:
 *
 * @Author: Jackchen
 * @Date: 2017-12-23 22:43
 * @Since 3.1.1
 */
public class ReceiveServer {

    private final int port;

    public ReceiveServer(int port){
        this.port = port;
    }

    public void start() throws Exception{
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap sb = new ServerBootstrap();
            sb.group(bossGroup,workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ReceiveServerInitializer());
            ChannelFuture cf = sb.bind(port).sync(); // 服务器异步创建绑定
            System.out.println(ReceiveServer.class + " started and listen on " + cf.channel().localAddress());
            cf.channel().closeFuture().sync(); // 关闭服务器通道
        } finally {
            bossGroup.shutdownGracefully().sync(); // 释放线程池资源
            workerGroup.shutdownGracefully().sync();
        }
    }
}
