package cn.sckj.scfront;

import cn.sckj.scfront.upload.SendClient;
import cn.sckj.scfront.util.ConfigProperties;
import com.alibaba.fastjson.JSON;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

public class GatherJob implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(GatherJob.class);

    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        GatherService gatherService = new GatherService();
        String message = JSON.toJSONString(gatherService.gather());
        ConfigProperties config = new ConfigProperties();
        System.out.println("send start send_flag="+config.isSend_flag());
        if(config.isSend_flag()) {
            System.out.println(config.getSend_server_ip() + "@" + config.getSend_server_port());
            SendClient client = new SendClient(config.getSend_server_ip(), config.getSend_server_port());
            System.out.println("send message:" + message);
            client.SendMessage(message);
        }
    }
}
