package cn.sckj.scfront.entity;

import java.util.List;

public class GatherInfo {

    //�ɼ�����
    private String gaterDate;

    private ComputerInfo computerInfo;

    private CpuInfo cpuInfo;

    private  List<DiskInfo> diskInfoList;

    private MemoryInfo memoryInfo;

    private NetInterfaceInfo netInterfaceInfo;

    public String getGaterDate() {
        return gaterDate;
    }

    public void setGaterDate(String gaterDate) {
        this.gaterDate = gaterDate;
    }

    public ComputerInfo getComputerInfo() {
        return computerInfo;
    }

    public void setComputerInfo(ComputerInfo computerInfo) {
        this.computerInfo = computerInfo;
    }

    public MemoryInfo getMemoryInfo() {
        return memoryInfo;
    }

    public void setMemoryInfo(MemoryInfo memoryInfo) {
        this.memoryInfo = memoryInfo;
    }

    public NetInterfaceInfo getNetInterfaceInfo() {
        return netInterfaceInfo;
    }

    public void setNetInterfaceInfo(NetInterfaceInfo netInterfaceInfo) {
        this.netInterfaceInfo = netInterfaceInfo;
    }

    public List<DiskInfo> getDiskInfoList() {
        return diskInfoList;
    }

    public void setDiskInfoList(List<DiskInfo> diskInfoList) {
        this.diskInfoList = diskInfoList;
    }

    public CpuInfo getCpuInfo() {
        return cpuInfo;
    }

    public void setCpuInfo(CpuInfo cpuInfo) {
        this.cpuInfo = cpuInfo;
    }
}
