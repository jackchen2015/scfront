package cn.sckj.scfront.entity;

/**
 * 功能简介:
 * 功能说明:
 *
 * @Author: Jackchen
 * @Date: 2017-12-20 11:14
 * @Since 3.1.1
 */
public class DiskInfo {
    //磁盘名称
    private String devname;
    //分区格式
    private String devtype;
    //总大小
    private String total;
    //剩余大小
    private String free;
    //可用大小
    private String avail;
    //使用量
    private String used;
    //读取
    private String diskreads;
    //写入
    private String diskwrites;

    public String getDevname() {
        return devname;
    }

    public void setDevname(String devname) {
        this.devname = devname;
    }

    public String getDevtype() {
        return devtype;
    }

    public void setDevtype(String devtype) {
        this.devtype = devtype;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getFree() {
        return free;
    }

    public void setFree(String free) {
        this.free = free;
    }

    public String getAvail() {
        return avail;
    }

    public void setAvail(String avail) {
        this.avail = avail;
    }

    public String getUsed() {
        return used;
    }

    public void setUsed(String used) {
        this.used = used;
    }


}
