package cn.sckj.scfront.entity;

/**
 * 功能简介: 手机CPU使用率
 * 功能说明:
 *
 * @Author: Jackchen
 * @Date: 2017-12-20 10:49
 * @Since 3.1.1
 */
public class CpuInfo {
    //总赫兹
    private String mhz;
    //厂商
    private String vendor;
    //类别
    private String model;
    //缓存大小
    private String cachesize;
    //用户使用率
    private String useruse;
    //系统使用率
    private String sysuse;
    //等待率
    private String wait;
    //错误率
    private String nice;
    //空闲率
    private String idle;
    //总的使用率
    private String combined;

    public String getMhz() {
        return mhz;
    }

    public void setMhz(String mhz) {
        this.mhz = mhz;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCachesize() {
        return cachesize;
    }

    public void setCachesize(String cachesize) {
        this.cachesize = cachesize;
    }

    public String getUseruse() {
        return useruse;
    }

    public void setUseruse(String useruse) {
        this.useruse = useruse;
    }

    public String getSysuse() {
        return sysuse;
    }

    public void setSysuse(String sysuse) {
        this.sysuse = sysuse;
    }

    public String getWait() {
        return wait;
    }

    public void setWait(String wait) {
        this.wait = wait;
    }

    public String getNice() {
        return nice;
    }

    public void setNice(String nice) {
        this.nice = nice;
    }

    public String getIdle() {
        return idle;
    }

    public void setIdle(String idle) {
        this.idle = idle;
    }

    public String getCombined() {
        return combined;
    }

    public void setCombined(String combined) {
        this.combined = combined;
    }
}
