package cn.sckj.scfront.entity;

/**
 * 功能简介: 收集的内存信息
 * 功能说明:
 *
 * @Author: Jackchen
 * @Date: 2017-12-20 10:48
 * @Since 3.1.1
 */
public class MemoryInfo {
    private String total;
    private String used;
    private String free;
    private String swaptotal;
    private String swapused;
    private String swapfree;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getUsed() {
        return used;
    }

    public void setUsed(String used) {
        this.used = used;
    }

    public String getFree() {
        return free;
    }

    public void setFree(String free) {
        this.free = free;
    }

    public String getSwaptotal() {
        return swaptotal;
    }

    public void setSwaptotal(String swaptotal) {
        this.swaptotal = swaptotal;
    }

    public String getSwapused() {
        return swapused;
    }

    public void setSwapused(String swapused) {
        this.swapused = swapused;
    }

    public String getSwapfree() {
        return swapfree;
    }

    public void setSwapfree(String swapfree) {
        this.swapfree = swapfree;
    }
}
