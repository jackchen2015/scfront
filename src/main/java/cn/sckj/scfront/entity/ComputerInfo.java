package cn.sckj.scfront.entity;

/**
 * 功能简介: 收集的电脑信息封装
 * 功能说明:
 *
 * @Author: Jackchen
 * @Date: 2017-12-20 10:43
 * @Since 3.1.1
 */
public class ComputerInfo {
    //用户名
    private String username;
    //计算机名
    private String computername;
    //计算机域名
    private String userdomain;
    //本地ip地址
    private String ip;
    //本地主机名
    private String hostname;
    //操作系统的名称
    private String osname;
    //操作系统的构架
    private String osarch;
    //操作系统的版本
    private String osversion;
    //操作系统名称
    private  String osvendername;
    //操作系统描述
    private String osdesc;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getComputername() {
        return computername;
    }

    public void setComputername(String computername) {
        this.computername = computername;
    }

    public String getUserdomain() {
        return userdomain;
    }

    public void setUserdomain(String userdomain) {
        this.userdomain = userdomain;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getOsname() {
        return osname;
    }

    public void setOsname(String osname) {
        this.osname = osname;
    }

    public String getOsarch() {
        return osarch;
    }

    public void setOsarch(String osarch) {
        this.osarch = osarch;
    }

    public String getOsversion() {
        return osversion;
    }

    public void setOsversion(String osversion) {
        this.osversion = osversion;
    }

    public String getOsvendername() {
        return osvendername;
    }

    public void setOsvendername(String osvendername) {
        this.osvendername = osvendername;
    }

    public String getOsdesc() {
        return osdesc;
    }

    public void setOsdesc(String osdesc) {
        this.osdesc = osdesc;
    }
}
