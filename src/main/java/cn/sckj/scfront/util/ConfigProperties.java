package cn.sckj.scfront.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigProperties {

    private  Properties configProperties = new Properties();

    private boolean receive_flag;

    private int receive_server_port;

    private boolean send_flag;

    private String send_server_ip;

    private int send_server_port;

    private String database_url;

    public boolean isReceive_flag() {
        return receive_flag;
    }

    public void setReceive_flag(boolean receive_flag) {
        this.receive_flag = receive_flag;
    }

    public boolean isSend_flag() {
        return send_flag;
    }

    public void setSend_flag(boolean send_flag) {
        this.send_flag = send_flag;
    }

    public String getSend_server_ip() {
        return send_server_ip;
    }

    public void setSend_server_ip(String send_server_ip) {
        this.send_server_ip = send_server_ip;
    }

    public int getSend_server_port() {
        return send_server_port;
    }

    public void setSend_server_port(int send_server_port) {
        this.send_server_port = send_server_port;
    }

    public int getReceive_server_port() {
        return receive_server_port;
    }

    public void setReceive_server_port(int receive_server_port) {
        this.receive_server_port = receive_server_port;
    }

    public String getDatabase_url() {
        return database_url;
    }

    public void setDatabase_url(String database_url) {
        this.database_url = database_url;
    }

    public ConfigProperties(){
        InputStream is = this.getClass().getResourceAsStream("/config.properties");
        try {
            configProperties.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        receive_flag = Boolean.parseBoolean(configProperties.getProperty("receive_flag"));
        receive_server_port = Integer.parseInt(configProperties.getProperty("receive_server_port"));
        send_flag = Boolean.parseBoolean(configProperties.getProperty("send_flag"));
        send_server_ip = configProperties.getProperty("send_server_ip");
        send_server_port = Integer.parseInt(configProperties.getProperty("send_server_port"));
        database_url = configProperties.getProperty("database_url");
    }
}
